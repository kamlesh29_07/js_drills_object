//function defaults(obj, defaultProps) {
    // Fill in undefined properties that match properties on the `defaultProps` parameter object.
    // Return `obj`.
    // http://underscorejs.org/#defaults
//}
export function defaults(obj, defaultProps){
   for(var name in defaultProps){
       if(!(obj.hasOwnProperty(name))){
           obj[name] = defaultProps[name];
       }
   }
}    