//function keys(obj) {
    // Retrieve all the names of the object's properties.
    // Return the keys as strings in an array.
    // Based on http://underscorejs.org/#keys
//}

export function keys(obj){
    const array =  [];
    for(var key in obj){
        array.push(key);
    }
    return array;
}