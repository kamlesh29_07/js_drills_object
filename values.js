//function values(obj) {
    // Return all of the values of the object's own properties.
    // Ignore functions
    // http://underscorejs.org/#values
//}

export function values(obj){
   let array = [];
   for(var key in obj){
       array.push(obj[key]);
   }
   return array;
}