//function pairs(obj) {
    // Convert an object into a list of [key, value] pairs.
    // http://underscorejs.org/#pairs
//}
export function pairs(obj){
    let array = [];
    for(var key in obj){
        array.push([key,obj[key]]);
    }
    return array;
}